def user_input(keys: list, selection):
    start = ''
    while start not in keys:
        start = input(selection)
    return start

print('''Welcome to MCadventure 
by Julian Heins and Marco Heins''')

start = user_input(['s', 'S'], 'press "S" to start: ')
    
print('''
You wake up and look around.
You can see cows, sheep and pigs.
There is a big mountain in front of you.
But then you notice something strange: All things are blocky.
You know it from a game, that you play a lot:
MINECRAFT
What do you do now? ''') 

step1 = user_input(['1', '2', '3'], selection='''\n Press:
    1 to climb up the mountain
    2 to go into the forest 
    3 to swim in the sea: ''')

if step1 == '1' :
    print('''\n When you got on top of the mountain,
you see a thing that looks like the nether portal that you know,
but it looks broken and you can see a chest.''')
